public class QuickSort {
	public void sort(int[] elements,int size) {	
		if(size>1) {
			int maxLoc=0;
			for(int i=1; i<size; i++) {//최대위치를 찾고
				if(elements[i]>elements[maxLoc])ss{
					maxLoc=i;sssss
				}
			}
			maxLoc++;
			swap(elements,maxLoc,size-1);//맨뒤와 최대위치를 swap
			quickSortRecursively(elements,0,size-1);//size-1만큼의 배열에 대하여 quickSort
		}
	}
	public void quickSortRecursively(int[] elements,int left,int right) {
		int pivot,up,down;
		if(left<right) {//파티션
			pivot=left;
			up=left;
			down=right+1;s
			do {
				do {//피봇보다 큰거찾음
					up++;
				}while(elements[pivot]>elements[up]);
				do {//피봇보다 작은거 찾음
					down--;
				}while(elements[pivot]<elements[down]);
				if(up<down) {//swap
					swap(elements,up,down);
				}
			}while(up<down);
			swap(elements,pivot,down);//피봇 위치에 맞게 swap
			int mid=down;
			quickSortRecursively(elements,left,mid-1);//피봇 왼쪽 quick sort
			quickSortRecursively(elements,mid+1,right);//피봇 오른쪽 quick sort
		}
	}	
	private void swap(int[] elements,int left,int right) {
		int temp=elements[left];
		elements[left]=elements[right];
		elements[right]=temp;
	}	
}
